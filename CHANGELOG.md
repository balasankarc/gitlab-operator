## 1.10.2 (2025-03-12)

No changes.

## 1.10.1 (2025-02-26)

No changes.

## 1.10.0 (2025-02-20)

### changed (11 changes)

- [Update ubi9-micro to d115f8a](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/17f045d64d531b14764b29de2c384fb71f6e55f4) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1082))
- [Update k8s.io dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/736c336ee3095518b47a8f68170559a91fd6b926) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1083))
- [Update ubi9-micro to 4a2052e](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/e0c51acc4921dba9b1401bc19441f5810ad73291) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1073))
- [Update prom-op-api/monitoring to v0.80.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/ff9b4f7cb268262903c5032bbc7edd63efdc217e) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1070))
- [Update Go from 1.23.5 to 1.23.6](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/d91d7622594b3298a6bb14590e7adc4d19b4c340) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1072))
- [Update golang.org/x/mod to v0.23.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/f406fb77866dff7a638e380e5182b17a9588b5b4) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1069))
- [Update ubi9-micro to a434c4b](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/40e9ea50557be1ffc2d7cbdac75adf97c5438d4b) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1068))
- [Update k8s.io dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/f9118c2d3c4501d5553d420d24976941e4ee1df4) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1054))
- [Update sigs.k8s.io/controller-runtime from v.0.19.4 to v0.20.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/5fef91fa7f6d8285193ee74f3505802b60dc4f30) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1061))
- [Update Go from 1.23.4 to 1.23.5](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/a1b48fb2cc758854f8145c39aecfde98fb28ad20) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1060))
- [Update ubi9-micro to f6e0a71](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/d54eaccecf999beb26b4edff642218c823656a4d) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1039))

### added (1 change)

- [Operator chart: Support custom zap-time-encoding](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/6fe72feff375cc48b362397f3396e3bbe4abda2f) by @thomasgl-orange ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1064))

## 1.9.2 (2025-02-12)

No changes.

## 1.9.1 (2025-01-22)

No changes.

## 1.9.0 (2025-01-16)

### changed (4 changes)

- [Update prom-op-api/monitoring to v0.79.2](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/2c37435d297024db44255c8518963e1e1950f89a) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1044))
- [Update Go from 1.23.3 to 1.23.4](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/bae171d967b030d4b87139cb7ce1996bb081857a) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1051))
- [Update k8s.io dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/cb93749e6965337f3e75eefc6d834f7de67c62a3) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1041))
- [Update testing dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/29ad297295496eb7f6de3a1bcc4769534bc5bcf3) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1040))

## 1.8.3 (2025-01-15)

No changes.

## 1.8.2 (2025-01-08)

No changes.

## 1.8.1 (2024-12-30)

No changes.

## 1.8.0 (2024-12-19)

### changed (9 changes)

- [Update testing dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/3310c996c06f98df82014f9753dbad96ec3734dc) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1036))
- [Update sigs.k8s.io/controller-runtime to v0.19.3](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/5e60b94d2fbbcada6f006fa14e5da8ba19afb10d) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1035))
- [Pin registry.access.redhat.com/ubi9-micro Docker tag to a410623](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/aec7af44c1dfe7799736fb8720871e1474d50ceb) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1034))
- [Update dependency danger-review to v2](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/f1702312a952828e96a9da6caabc9b7e68a162aa) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1033))
- [Update golang.org/x/mod to v0.22.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/dc380e3ab862d68ea449b247f41ff9a0c7a1b361) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1021))
- [Update k8s.io API and client libraries](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/38d29a07a5543cbd72a055914fe427978a3fbcdd) by @gitlab-dependency-update-bot ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1026))
- [Update prom-op-api/monitoring to v0.78.2](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/18d159bb23f4180c3d41cfc30cb2a8aad54efe20) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1027))
- [Update github.com/Masterminds/semver/v3 to v3.3.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/4ae7fe52046e2497ef72580792cfdf460a809fd8) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1023))
- [Update registry.access.redhat.com/ubi9-micro Docker tag to v9.5](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/5a9811f00259e34950297edeea63a096f5fc40a0) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1022))

## 1.7.2 (2024-12-11)

No changes.

## 1.7.1 (2024-11-26)

No changes.

## 1.7.0 (2024-11-21)

### changed (5 changes)

- [Remove CI testing for Kubernetes 1.27 and 1.28](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/ad817c161ff849e8a5758e8c79e15f5931bc32ee) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1014))
- [Update Go from 1.23.2 to 1.23.3](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/7b3dad70dd015afe19c07a30da376a058eace492) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1020))
- [Update prom-op-api/monitoring to v0.78.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/ed7468877b4c45a88b07201e94a70f057f1c5c1e) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/981))
- [Update ginkgo and gomega](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/9faf2f9ab02b5d10826b9b87b2eaa2a69f208663) by @gitlab-dependency-update-bot ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1008))
- [Update k8s.io/utils to 6fe5fd8](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/472733478cc565df78700be39a792485293fb342) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1013))

### fixed (1 change)

- [Fix non-overridable certmanager annotation on Ingress objects](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/fc423e806eccb10b2ba8b32af6bcfae2125c1b3e) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1007))

## 1.6.1 (2024-11-13)

No changes.

## 1.6.0 (2024-10-31)

### changed (3 changes)

- [Update k8s.io](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/0986e21780ecbe0534bee773a61aeac16037ec86) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1002))
- [Update github.com/cert-manager/cert-manager to v1.16.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/60a3049eea25bc11c3d13a82b80a09e2a9e30f95) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/998))
- [Update github.com/cert-manager/cert-manager to v1.16.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/4269eda49dc8eef59a7d58f423196fc60f9ed2dc) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/992))

### added (3 changes)

- [Add support for KAS PodMonitor](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/b274fd25dd397dd889bdda1ecd3a1ec4d017565c) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/995))
- [Add support for OpenShift 4.17](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/3a1ffcf2fb12468d79ef30a35ddd7202393fbc6d) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/1000))
- [Add support for GitLab Geo](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/8710695add3e4c33437c25d00e8556967255588f) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/960))

## 1.5.1 (2024-10-23)

No changes.

## 1.5.0 (2024-10-17)

### changed (4 changes)

- [Update Go from 1.23.1 to 1.23.2](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/c84ecb7a75b4ad1d76e96e355fa39d7c52da2924) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/990))
- [Update golang.org/x/mod to v0.21.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/2e3aaa5ae3da0ab36b551a5a74cbc3275a95f45d) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/971))
- [Update k8s.io/utils to 49e7df5](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/84d88fa6cfebbd596656a331a150525226e3c54e) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/982))
- [Migrate from kube-rbac-proxy to authn/authz filter](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/9fcae9758a23ed910b3e4792e32527467ca02014) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/977))

## 1.4.2 (2024-10-09)

No changes.

## 1.4.1 (2024-09-25)

No changes.

## 1.4.0 (2024-09-19)

### changed (11 changes)

- [Update k8s.io](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/3f300781ab4552c6590408b99904eed92ae37e0c) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/979))
- [Update prom-op-api/monitoring to v0.76.2](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/fae5a8bb5705ebefb02b69592906466e9acda484) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/970))
- [Update Go from 1.23.0 to 1.23.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/21f98d2eb86966f17cebd63ff7dc5e400e153f3f) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/974))
- [Update github.com/Masterminds/semver/v3 to v3.3.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/976e966b9b74046d9a54dd5338661bed3b2b49f5) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/967))
- [Update testing](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/11d5ee3ffc2cc1e1cc81cc757e873a4b1311fdc0) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/968))
- [Update github.com/onsi/ginkgo/v2 to v2.20.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/872ca8a927550c601547d56f9ca6fd9a677ebd2f) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/964))
- [Update dario.cat/mergo to v1.0.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/e441d8b57d693d37270dcdf22b01d6b1e97ccf85) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/955))
- [Update k8s.io dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/209c82f19bdfa534cdff6804b1c9c6617dcf08fc) by @gitlab-dependency-update-bot ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/949))
- [Update github.com/cert-manager/cert-manager to v1.15.3](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/f619b60bfae189eabee1b207cafedc7e36a6eb8c) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/953))
- [Update Go to 1.23](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/9f952caafb5390b44e3f56e5164713fc4a8bc213) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/952))
- [Update prom-op-api/monitoring to v0.76.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/ab18ae9c569ac4c7e7e73d0a452ff95f22a94490) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/950))

### added (1 change)

- [Support NGINX Ingress for Geo traffic](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/a21ab91043386e55350081b649e7784ff5800202) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/959))

## 1.3.3 (2024-09-17)

No changes.

## 1.3.2 (2024-09-11)

No changes.

## 1.3.1 (2024-08-21)

No changes.

## 1.3.0 (2024-08-15)

### changed (18 changes)

- [Update sigs.k8s.io/controller-runtime to v0.18.5](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/077c60a5b220d639f016f6f5e234bbfef62079be) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/947))
- [Update golang.org/x/exp to 0cdaa3a](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/c88fe7069756fe613b6505821614be5e00283759) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/945))
- [Update k8s.io/kube-openapi to 8e68654](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/a8c9f44405f52ea6e711aeb5017fb0599683b6d9) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/944))
- [Update Go from 1.22.5 to 1.22.6](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/3ecb6ca3e7000d341ca0cf7bf4e75ade0d3dd636) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/943))
- [Update github.com/onsi/ginkgo/v2 to v2.20.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/ecf5ee33eba108bd6ea24858c3cca52a18abb28a) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/941))
- [Update golang.org/x/mod to v0.20.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/130b21d1e3e17f067a76aa27332504b79edcee58) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/937))
- [Update k8s.io/kube-openapi to 7a9a4e8](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/1266c54f79694f3e1036dfb9bb59373c74f1ba70) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/931))
- [Update indirect](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/e61dcd5b6b5c23a36fdcfe4d90eb92369bd9d6cf) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/920))
- [Update github.com/cert-manager/cert-manager to v1.15.2](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/0e710eb5df0fa4fa6032180e73268d10ba07361d) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/935))
- [Update testing](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/93055709311107a45ce212e9d81667e7cfdd11c3) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/933))
- [Update github.com/onsi/gomega to v1.34.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/82c30487282172fccadb5c4da9e42155430d5942) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/930))
- [Update indirect k8s.io dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/b393dc2fb207923038f4a0a8a2ad737da8c9de41) by @gitlab-dependency-update-bot ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/917))
- [Update prom-op-api/monitoring to v0.75.2](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/c3d0f6ab8d65db7748507ff662fe165a164bcf57) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/918))
- [Update indirect dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/934900b4232eddcef438c73d0fec12b4706773db) by @gitlab-dependency-update-bot ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/915))
- [Update golang.org/x/exp digest to e3f2596](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/0ec8b56f182039bc07a0aaec05501b5d06da164e) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/887))
- [Update k8s.io to v0.30.3](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/864b9d01fd06d6ada97a21f2c8623025b1178592) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/889))
- [Add support for OpenShift 4.16](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/dd40ad8275128f402e89ad058a7f614d5ffe1a8e) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/890))
- [Update module prometheus-operator/monitoring to v0.75.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/db676b23f94e6dbb7dcd7736fa495cd050025ae6) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/861))

## 1.2.2 (2024-08-07)

No changes.

## 1.2.1 (2024-07-25)

No changes.

## 1.2.0 (2024-07-18)

### changed (12 changes)

- [Update k8s.io/utils digest to 18e509b](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/a6b006acbe148a36fec57c1a391862d131bd3437) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/883))
- [Move all workloads to `nonroot-v2` SCC](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/7a009fa7f2ed672bf4003077cf743527980d5eb2) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/838))
- [Update golang.org/x/exp digest to 46b0784](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/5a85097d2af8f51da5072eea0917f42f6607415a) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/878))
- [Update Go from 1.22.4 to 1.22.5](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/2e3e5e6569656839303b2b58e8e9dd82ab8fd6b4) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/871))
- [Update dependency danger-review to v1.4.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/e6cdf30d92463b2bad92f2ec5d7c7395b33aecae) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/869))
- [Update module github.com/cert-manager/cert-manager to v1.15.1](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/f2f23bc8f77472b49e5adcef5d70618eeeebaa3d) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/865))
- [Deprecate support for Kubernetes 1.25 and 1.26](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/cdedd67845efe498cc49a57758e65d48aefc803a) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/857))
- [Update module github.com/onsi/ginkgo/v2 to v2.19.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/3a3a3d470d470710dd7b02577847d5fd1f12ef25) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/856))
- [Update golang.org/x/exp digest to 7f521ea](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/782da95a1e47e7d9fc265335c02b7ffb9e1d876d) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/853))
- [Update cert-manager from 1.6.1 to 1.15.0](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/5ed0059a33aa320576c153b1ef26cd4379e0b321) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/846))
- [Update UBI micro base image from 8.10 to 9.4](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/ec4a9a72bf19c199815f43ce9c17f4597873984d) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/849))
- [Update Go dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/4c5b0796a3b70f4ac047390d854ecd62ecd55d73) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/845))

### fixed (1 change)

- [Fix support for Zoekt](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/4682de07d011af41341f956a011c4fcceb92913e) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/850))

## 1.1.3 (2024-07-24)

No changes.

## 1.1.2 (2024-07-10)

No changes.

## 1.1.1 (2024-06-26)

No changes.

## 1.1.0 (2024-06-20)

### changed (3 changes)

- [Update Go from 1.21 to 1.22](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/db81731407111f3804d3a4ac2befdba11d57cc04) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/825))
- [Update go dependencies](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/54a6c9b1d4e6d3d3d2cda7728db9d98e14d4de95) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/820))
- [Base final image on ubi micro instead of minimal](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/commit/724d505da2c6dcd4ed7283ff006578d4e309f426) ([merge request](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/merge_requests/821))

## 1.0.2 (2024-06-12)

No changes.

## 1.0.1 (2024-05-22)

No changes.

## 1.0.0 (2024-05-16)

### added (1 change)

- [[CI] Add Kubernetes 1.27 CI Jobs](gitlab-org/cloud-native/gitlab-operator@8ed5da92bf936a1058e2a8e272e7d604b6237228) ([merge request](gitlab-org/cloud-native/gitlab-operator!795))

### deprecated (1 change)

- [Removed OpenShift 4.11 tests](gitlab-org/cloud-native/gitlab-operator@314d1efdf5b1e3512ab56dbf2d5acbedc935e270) ([merge request](gitlab-org/cloud-native/gitlab-operator!750))

### fixed (1 change)

- [Fix specs checking for MinIO create buckets Job](gitlab-org/cloud-native/gitlab-operator@9b77664b8adecbaa1f1615e09f2d41c7b3b97f05) ([merge request](gitlab-org/cloud-native/gitlab-operator!791))

## 0.31.2 (2024-05-08)

No changes.

## 0.31.1 (2024-04-24)

No changes.

## 0.31.0 (2024-04-18)

### changed (1 change)

- [Change 'redis.password' key to 'redis.auth'](gitlab-org/cloud-native/gitlab-operator@b002c6d08a58f7a9d22ced48c07d71045dfdae06) ([merge request](gitlab-org/cloud-native/gitlab-operator!786))

## 0.30.3 (2024-04-15)

No changes.

## 0.30.2 (2024-04-10)

No changes.

## 0.30.1 (2024-03-27)

No changes.

## 0.30.0 (2024-03-21)

### fixed (2 changes)

- [Don't override Deployment replica counts of zero](gitlab-org/cloud-native/gitlab-operator@151f1be140d2381e246c32c6be3078a212648f5b) ([merge request](gitlab-org/cloud-native/gitlab-operator!752))
- [Resource cleanup: Don't error if object not found](gitlab-org/cloud-native/gitlab-operator@00749d7c610914453d0d1a482e1a2c740e5df9b2) ([merge request](gitlab-org/cloud-native/gitlab-operator!754))

## 0.29.2 (2024-03-06)

No changes.

## 0.29.1 (2024-02-21)

No changes.

## 0.29.0 (2024-02-15)

### added (1 change)

- [Helm Chart: support log level configuration](gitlab-org/cloud-native/gitlab-operator@cffdf796bb2e4e990d68335a6a34fa9978130a4d) ([merge request](gitlab-org/cloud-native/gitlab-operator!749))

### changed (1 change)

- [Update Go to 1.21](gitlab-org/cloud-native/gitlab-operator@529eeee6f5fd67c78b1d625260e2bebf1fbff653) ([merge request](gitlab-org/cloud-native/gitlab-operator!740))

### fixed (1 change)

- [Add support for Toolbox's restore PersistentVolumeClaim](gitlab-org/cloud-native/gitlab-operator@8f373c725f7705409a927b6cf8c316b6e128684c) ([merge request](gitlab-org/cloud-native/gitlab-operator!731))

### other (1 change)

- [Annotate ClusterServiceVersion with feature annotations](gitlab-org/cloud-native/gitlab-operator@1671ae92ef478bc96d0c4a03bfd303eb3a597670) ([merge request](gitlab-org/cloud-native/gitlab-operator!739))

## 0.28.2 (2024-02-07)

No changes.

## 0.28.1 (2024-01-25)

No changes.

## 0.28.0 (2024-01-18)

No changes.

## 0.27.2 (2024-01-13)

No changes.

## 0.27.1 (2024-01-11)

No changes.

## 0.27.0 (2023-12-21)

### changed (1 change)

- [Support OpenShift 4.14](gitlab-org/cloud-native/gitlab-operator@67f75018b5490493a96272d67c0e0646ec0d4654) ([merge request](gitlab-org/cloud-native/gitlab-operator!723))

### added (1 change)

- [Build and publish arm64 container image](gitlab-org/cloud-native/gitlab-operator@f02d8d019618a993e28b0cfecaa4262de6b770eb) ([merge request](gitlab-org/cloud-native/gitlab-operator!707))

## 0.26.2 (2023-12-13)

No changes.

## 0.26.1 (2023-11-30)

No changes.

## 0.26.0 (2023-11-16)

### changed (2 changes)

- [Use Service and PodMonitors from the Helm template](gitlab-org/cloud-native/gitlab-operator@d05b4f16881344e009ceef0623ff4136067f8f0c) ([merge request](gitlab-org/cloud-native/gitlab-operator!661))
- [Migrations for Operator SDK 1.32.0](gitlab-org/cloud-native/gitlab-operator@6c15e3d27d2510f71fae6adabd9c63bffc46cb55) ([merge request](gitlab-org/cloud-native/gitlab-operator!702))

## 0.25.2 (2023-11-14)

No changes.

## 0.25.1 (2023-10-31)

No changes.

## 0.25.0 (2023-10-22)

### added (2 changes)

- [Add Option to Configure Container securityContext](gitlab-org/cloud-native/gitlab-operator@434a77c30f1aa86b7fd7286685d40bc1c8a046d5) by @omland-94 ([merge request](gitlab-org/cloud-native/gitlab-operator!697))
- [Add support for GitLab Zoekt](gitlab-org/cloud-native/gitlab-operator@1dbc8eee8e3754496eb37069e0c2bef0c4c66c96) ([merge request](gitlab-org/cloud-native/gitlab-operator!690))

### changed (1 change)

- [Replace Prometheus object definitions with objects from charts](gitlab-org/cloud-native/gitlab-operator@f21b17dc5b6905e1fac4cfb7e547e5a5e84b9b36) ([merge request](gitlab-org/cloud-native/gitlab-operator!686))

### fixed (3 changes)

- [Populate ACME Ingresse to managed objects](gitlab-org/cloud-native/gitlab-operator@27b69485a7aa4a5392a851774f83110798c25c32) ([merge request](gitlab-org/cloud-native/gitlab-operator!699))
- [Fix modified chart catalog after a chart was rendered](gitlab-org/cloud-native/gitlab-operator@ee2cb71efdfd6ef6c751e6e70cc23220dc32954c) ([merge request](gitlab-org/cloud-native/gitlab-operator!694))
- [Support multiple Services from GitLab Pages](gitlab-org/cloud-native/gitlab-operator@1b6627bfb742f6cc4e81c517233814265a7f4a69) ([merge request](gitlab-org/cloud-native/gitlab-operator!698))

### other (1 change)

- [Replace apimachinery.Scheme with kubectl.Scheme](gitlab-org/cloud-native/gitlab-operator@99286ed2f77244db2a3e44d1d95c06d01cc8ad73) ([merge request](gitlab-org/cloud-native/gitlab-operator!692))

## 0.24.1 (2023-09-28)

No changes.

## 0.24.0 (2023-09-22)

### fixed (1 change)

- [Fix monitoring resources create / delete loop](gitlab-org/cloud-native/gitlab-operator@53f73f98c71f7ef39f4398f08e48a9a2a8bf02e4) ([merge request](gitlab-org/cloud-native/gitlab-operator!681))

### added (1 change)

- [Add Kubernetes 1.26 CI test jobs](gitlab-org/cloud-native/gitlab-operator@8c75c28beee70616affaf255408a370586f857dd) ([merge request](gitlab-org/cloud-native/gitlab-operator!678))

## 0.23.4 (2023-09-18)

No changes.

## 0.23.3 (2023-09-12)

### fixed (1 change)

- [Fix monitoring resources create / delete loop](gitlab-org/cloud-native/gitlab-operator@2de341ae14300b0393366984efaa2d006ef54a88) ([merge request](gitlab-org/cloud-native/gitlab-operator!684))

## 0.23.2 (2023-09-05)

No changes.

## 0.23.1 (2023-08-31)

No changes.

## 0.23.0 (2023-08-22)

### other (2 changes)

- [Update client-go to 0.27.0](gitlab-org/cloud-native/gitlab-operator@8c5237230fb003c32f02d43ac0f0cc13a51fafed) ([merge request](gitlab-org/cloud-native/gitlab-operator!664))
- [Revert Set global.hosts.externalIP on NGINX Service](gitlab-org/cloud-native/gitlab-operator@a6c1db6dde79352029a683bbe55c81959fc2ba9c) ([merge request](gitlab-org/cloud-native/gitlab-operator!652))

### fixed (2 changes)

- [Delete unused resources](gitlab-org/cloud-native/gitlab-operator@14877813ba2b90ea43fa4cc074baf3e14a5433c9) ([merge request](gitlab-org/cloud-native/gitlab-operator!649))
- [Remove override for GitLab Shell Service type](gitlab-org/cloud-native/gitlab-operator@b94d581089873cad6037756eddfe387018fbd622) ([merge request](gitlab-org/cloud-native/gitlab-operator!653))

## 0.22.4 (2023-08-11)

No changes.

## 0.22.3 (2023-08-03)

No changes.

## 0.22.2 (2023-08-01)

No changes.

## 0.22.1 (2023-07-25)

No changes.

## 0.22.0 (2023-07-22)

### fixed (2 changes)

- [Use createOrPatch for Issuer, not createOrUpdate](gitlab-org/cloud-native/gitlab-operator@8163e95ebe4a2aaf672a0e3ece3ef79c5a66197c) ([merge request](gitlab-org/cloud-native/gitlab-operator!650))
- [Discover cluster capabilities and use them in chart](gitlab-org/cloud-native/gitlab-operator@7daf0b8e818cf387d6e55a2e5d904dac8188f9ee) ([merge request](gitlab-org/cloud-native/gitlab-operator!644))

### changed (1 change)

- [Update rbac-proxy to 0.14.1](gitlab-org/cloud-native/gitlab-operator@a135c110fee5b24c29e123710edc41b86f9d98bc) ([merge request](gitlab-org/cloud-native/gitlab-operator!641))

### added (1 change)

- [Add OwnerReference filtering to DiscoverManagedObjects](gitlab-org/cloud-native/gitlab-operator@217882ef6debba68c3b162bc675fc7319378b00b) ([merge request](gitlab-org/cloud-native/gitlab-operator!627))

## 0.21.2 (2023-07-05)

No changes.

## 0.21.1 (2023-06-29)

No changes.

## 0.21.0 (2023-06-22)

### added (2 changes)

- [Add auto-discovery feature to DiscoverManagedObjects](gitlab-org/cloud-native/gitlab-operator@04f1667724606ca5ef479848c4c938b40a75752a) ([merge request](gitlab-org/cloud-native/gitlab-operator!626))
- [Add utility for discovering managed objects](gitlab-org/cloud-native/gitlab-operator@40055c16d8382cf29a9e438ec48e6f622204732b) ([merge request](gitlab-org/cloud-native/gitlab-operator!546))

### fixed (3 changes)

- [Fix review environment DNS labels](gitlab-org/cloud-native/gitlab-operator@31889132e9d91d9f84ea96402219a58d4b293e69) ([merge request](gitlab-org/cloud-native/gitlab-operator!623))
- [Fix PostgreSQL and Redis resource lookup after chart upgrade](gitlab-org/cloud-native/gitlab-operator@708523dcf2ceb055008b9ad096394e1f204d38d2) ([merge request](gitlab-org/cloud-native/gitlab-operator!619))
- [Fix long running reconciliation loop on upgrade](gitlab-org/cloud-native/gitlab-operator@67d1f10f6a38d2450e6e1843a21c88819147ea2c) ([merge request](gitlab-org/cloud-native/gitlab-operator!617))

## 0.20.6 (2023-06-16)

No changes.

## 0.20.5 (2023-06-08)

No changes.

## 0.20.4 (2023-06-07)

No changes.

## 0.20.3 (2023-06-05)

- [Fix PostgreSQL and Redis resource lookup after chart upgrade](gitlab-org/cloud-native/gitlab-operator/@f65e60c6d985e39945224b49bf1fc39b24f7f656) ([merge request](gitlab-org/cloud-native/gitlab-operator!619))

## 0.20.2 (2023-06-05)

No changes.

## 0.20.1 (2023-05-23)

No changes.

## 0.20.0 (2023-05-22)

### other (1 change)

- [Documenting switching back to single connection](gitlab-org/cloud-native/gitlab-operator@77a8854c1f6c7cb58da18c08870fd897c8d2e811) ([merge request](gitlab-org/cloud-native/gitlab-operator!616))

## 0.19.4 (2023-05-17)

No changes.

## 0.19.3 (2023-05-10)

No changes.

## 0.19.2 (2023-05-05)

No changes.

## 0.19.1 (2023-05-02)

No changes.

## 0.19.0 (2023-04-22)

### added (1 change)

- [Add support for nameOverride of redis resources](gitlab-org/cloud-native/gitlab-operator@1dcd2610e1e0e83b04d56497ea837244234898b3) ([merge request](gitlab-org/cloud-native/gitlab-operator!565))

### fixed (1 change)

- [Fix linter issue for doc/openshift_ingress.md](gitlab-org/cloud-native/gitlab-operator@8fc3e4797ead02f58ec01a218b94dda55c3637e0) ([merge request](gitlab-org/cloud-native/gitlab-operator!610))

## 0.18.3 (2023-04-14)

No changes.

## 0.18.2 (2023-04-05)

No changes.

## 0.18.1 (2023-03-30)

No changes.

## 0.18.0 (2023-03-22)

### other (1 change)

- [Distinguish operator default and operator override values](gitlab-org/cloud-native/gitlab-operator@02b42fab33163a2f2434e8a4da0c7a6e26c539dd) ([merge request](gitlab-org/cloud-native/gitlab-operator!586))

## 0.17.3 (2023-03-09)

No changes.

## 0.17.2 (2023-03-02)

No changes.

## 0.17.1 (2023-02-24)

No changes.

## 0.17.0 (2023-02-22)

### other (1 change)

- [Use non-floating golangci-lint image](gitlab-org/cloud-native/gitlab-operator@0f8e5094e5794882e522c10f9d0025d12e115595) ([merge request](gitlab-org/cloud-native/gitlab-operator!584))

### added (1 change)

- [Add support for nameOverride of PostgreSQL resources](gitlab-org/cloud-native/gitlab-operator@993bc94c977c419c827daf4b65dfa65f1bb23fff) ([merge request](gitlab-org/cloud-native/gitlab-operator!570))

### fixed (2 changes)

- [Fail early if Chart catalog cannot be populated](gitlab-org/cloud-native/gitlab-operator@089e4204bf848f24cb918f8b8ddaf549fa723769) ([merge request](gitlab-org/cloud-native/gitlab-operator!572))
- [Truncate secret annotation key](gitlab-org/cloud-native/gitlab-operator@792b8bcf6276fe6d398125268b367b1250d1a8ff) ([merge request](gitlab-org/cloud-native/gitlab-operator!569))

## 0.16.3 (2023-02-15)

No changes.

## 0.16.2 (2023-02-14)

No changes.

## 0.16.1 (2023-01-31)

No changes.

## 0.16.0 (2023-01-22)

### added (1 change)

- [Add OLM bundle testing script with instructions](gitlab-org/cloud-native/gitlab-operator@dd1c21081d8d1152e1cd8c5ca433f093cda4528e) ([merge request](gitlab-org/cloud-native/gitlab-operator!352))

### fixed (2 changes)

- [Support disabling webhook self-signed cert](gitlab-org/cloud-native/gitlab-operator@a14b87b3b9bc31ce860c1c5a7eed63cfbb0613c9) ([merge request](gitlab-org/cloud-native/gitlab-operator!562))
- [Create cert manager resources only when needed ](gitlab-org/cloud-native/gitlab-operator@0f2df982efda0c51e3680c7aaa6a9d07ee270477) by @javion1 ([merge request](gitlab-org/cloud-native/gitlab-operator!561))

## 0.15.5 (2023-01-17)

No changes.

## 0.15.4 (2023-01-17)

No changes.

## 0.15.3 (2023-01-11)

No changes.

## 0.15.2 (2023-01-09)

No changes.

## 0.15.1 (2023-01-05)

No changes.

## 0.15.0 (2022-12-22)

No changes.

## 0.14.2 (2022-12-06)

No changes.

## 0.14.1 (2022-11-30)

No changes.

## 0.14.0 (2022-11-22)

### removed (1 change)

- [Change OpenShift minimum version to 4.8](gitlab-org/cloud-native/gitlab-operator@75a81e8cde8e57ece8a2fd24b42fc5bb6c736e71) ([merge request](gitlab-org/cloud-native/gitlab-operator!545))

### fixed (1 change)

- [Add OCO setup script](gitlab-org/cloud-native/gitlab-operator@14105d6d280ff20bf17183009819ba35c26cae0b) ([merge request](gitlab-org/cloud-native/gitlab-operator!533))

## 0.13.4 (2022-11-14)

No changes.

## 0.13.3 (2022-11-08)

No changes.

## 0.13.2 (2022-11-02)

No changes.

## 0.13.1 (2022-10-24)

No changes.

## 0.13.0 (2022-10-22)

### fixed (1 change)

- [Ensure "Running" phase only set if Condition true](gitlab-org/cloud-native/gitlab-operator@b6f8a80f22b8515fde666ee423e3e01d0994c4bd) ([merge request](gitlab-org/cloud-native/gitlab-operator!539))

### added (4 changes)

- [Add documentation on certified images](gitlab-org/cloud-native/gitlab-operator@fb664e38d788b05e423296a32268233d64509408) ([merge request](gitlab-org/cloud-native/gitlab-operator!537))
- [Support reconciling the spamcheck chart](gitlab-org/cloud-native/gitlab-operator@6e6da19e052a549da62d145d7aa4333252add7e6) ([merge request](gitlab-org/cloud-native/gitlab-operator!536))
- [Support batch/v1beta1 and batch/v1 for CronJob](gitlab-org/cloud-native/gitlab-operator@4a52125d1423c3a13dfbc3b5dfb792234f9445f3) by @Omar007 ([merge request](gitlab-org/cloud-native/gitlab-operator!532))
- [Add new features and components to new GitLab resource adapter](gitlab-org/cloud-native/gitlab-operator@1ae8cda74c28d572b720d4445c514afb6a0b4053) ([merge request](gitlab-org/cloud-native/gitlab-operator!527))

### removed (1 change)

- [Remove the unused custom resource adapter](gitlab-org/cloud-native/gitlab-operator@535d0641b23dfbeee4b221f961f4cb07a6fdc17a) ([merge request](gitlab-org/cloud-native/gitlab-operator!529))

### changed (1 change)

- [Replace the old adapter with the new one](gitlab-org/cloud-native/gitlab-operator@9b9eaf01068087317527518e5722d1e95a67e24f) ([merge request](gitlab-org/cloud-native/gitlab-operator!528))

## 0.12.3 (2022-10-19)

No changes.

## 0.12.2 (2022-10-04)

No changes.

## 0.12.1 (2022-09-29)

No changes.

## 0.12.0 (2022-09-22)

### fixed (1 change)

- [Add fixes from manual run of 0.10.2 certification](gitlab-org/cloud-native/gitlab-operator@7c0368f587c166bb82870e4a33cda9c7ed0eefb9) ([merge request](gitlab-org/cloud-native/gitlab-operator!511))

### performance (1 change)

- [Add `jobSucceeded` method to check Job status](gitlab-org/cloud-native/gitlab-operator@d6c37cf6ed736777a94ac2519c51dcdbac704e49) ([merge request](gitlab-org/cloud-native/gitlab-operator!503))

### other (1 change)

- [Remove NGINX DefaultBackend from tests](gitlab-org/cloud-native/gitlab-operator@1604313afffe233ae427a2092e704647b7bf8f6d) ([merge request](gitlab-org/cloud-native/gitlab-operator!514))

## 0.11.4 (2022-09-05)

No changes.

## 0.11.3 (2022-08-30)

No changes.

## 0.11.2 (2022-08-23)

No changes.

## 0.11.1 (2022-08-22)

No changes.

## 0.11.0 (2022-08-22)

### security (1 change)

- [Add separate nonroot and anyuid RBAC](gitlab-org/cloud-native/gitlab-operator@01d49a714d62cf8d38220e707edc69f9f71a17ce) ([merge request](gitlab-org/cloud-native/gitlab-operator!447))

### added (3 changes)

- [Add Vale configuration and style references](gitlab-org/cloud-native/gitlab-operator@1546a091cd5ad38166314ffb7cc0cdd22df2ff96) ([merge request](gitlab-org/cloud-native/gitlab-operator!509))
- [Script and document RedHat certification process](gitlab-org/cloud-native/gitlab-operator@cdd3b1ed180434e88054079391ca0d0965ccf0f8) ([merge request](gitlab-org/cloud-native/gitlab-operator!494))
- [Add GKE 1.22 jobs](gitlab-org/cloud-native/gitlab-operator@ecdc70c91cf9f14a1eb3dab68135428c2316de69) ([merge request](gitlab-org/cloud-native/gitlab-operator!497))

### changed (1 change)

- [Use project token for RH certification jobs](gitlab-org/cloud-native/gitlab-operator@3cde0d00e1051a306850b102b3b62bd31af7c34a) ([merge request](gitlab-org/cloud-native/gitlab-operator!505))

### fixed (1 change)

- [Deep copy Chart values for catalog query](gitlab-org/cloud-native/gitlab-operator@9b231838685be534e68d40aab69a30cd1970e5c8) ([merge request](gitlab-org/cloud-native/gitlab-operator!499))

## 0.10.1 (2022-07-28)

No changes.

## 0.10.0 (2022-07-22)

### other (1 change)

- [Add .task/ to gitignore](gitlab-org/cloud-native/gitlab-operator@318e1a386eca5970960e166dcab053b1efab9b26) ([merge request](gitlab-org/cloud-native/gitlab-operator!481))

## 0.9.3 (2022-07-19)

No changes.

## 0.9.2 (2022-07-05)

No changes.

## 0.9.1 (2022-06-30)

No changes.

## 0.9.0 (2022-06-22)

No changes.

## 0.8.2 (2022-06-16)

No changes.

## 0.8.1 (2022-06-01)

No changes.

## 0.8.0 (2022-05-22)

No changes.

## 0.7.2 (2022-05-05)

No changes.

## 0.7.1 (2022-05-02)

No changes.

## 0.7.0 (2022-04-22)

No changes.
